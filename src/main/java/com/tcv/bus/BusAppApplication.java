package com.tcv.bus;

import com.tcv.bus.dao.ArretRepository;
import com.tcv.bus.dao.EscaleRepository;
import com.tcv.bus.dao.TrajetRepository;
import com.tcv.bus.dao.VilleRepository;
import com.tcv.bus.entities.*;
import com.tcv.bus.utils.ArretCreator;
import com.tcv.bus.utils.BusCreator;
import com.tcv.bus.utils.TrajetCreator;
import com.tcv.bus.utils.VilleCreator;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.*;

@SpringBootApplication
public class BusAppApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(BusAppApplication.class, args);
    }
    @Bean
    public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
        VelocityEngineFactory velocityEngineFactory = new VelocityEngineFactory();
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        velocityEngineFactory.setVelocityProperties(props);
        return velocityEngineFactory.createVelocityEngine();
    }
    @Autowired
    private VilleRepository villeRepository;
    @Autowired
    private TrajetRepository trajetRepository;
    @Autowired
    private EscaleRepository escaleRepository;
    @Override
    public void run(String... args) throws Exception {
        Bus bus1 = BusCreator.createBus("Flixbus",62,"516518");
        Bus bus2 = BusCreator.createBus("Ouibus",58,"78856");
        Bus bus3 = BusCreator.createBus("EuroLine",45,"237657");
        Bus bus4 = BusCreator.createBus("Blablabus",70,"045676");



        Ville ville1 = VilleCreator.createVille("Paris");
        Ville ville2 = VilleCreator.createVille("Montpellier");
        Ville ville3 = VilleCreator.createVille("Marseille");

        Arret arret11 = ArretCreator.createArret(ville1,2.3721886,48.8443073,"Gare de Lyon");
        Arret arret12 = ArretCreator.createArret(ville1,2.3181892,48.8410238,"Gare Montparnasse");
        Arret arret13 = ArretCreator.createArret(ville1,2.3553137,48.8809481,"Gare du nord");

        Escale escale1= new Escale();
        Escale escale2= new Escale();
        escale1.setStep(Escale.STEP_1);
        escale2.setStep(Escale.STEP_2);

        List<Escale> escaleList = new ArrayList<>();
        escaleList.add(escale1);
        escaleList.add(escale2);
        Arret arret21 = ArretCreator.createArret(ville2,3.918221,43.6035139,"Odysseum");
        Arret arret22 = ArretCreator.createArret(ville2,3.8578945,43.583769,"Sabines");
        Arret arret23 = ArretCreator.createArret(ville2,3.9630485,43.579603,"Aeroport");


        Arret arret31 = ArretCreator.createArret(ville3,5.3557599,43.2944799,"Vieux port");
        Arret arret32 = ArretCreator.createArret(ville3,5.3778013,43.3040849,"Saint-Charles");
        Arret arret33 = ArretCreator.createArret(ville3,5.2349719,43.4417399,"Aeroport");

       // villeRepository.saveAll(Arrays.asList(ville1,ville2,ville3));

        Trajet trajet1 =TrajetCreator.createTrajet(arret11,arret31,new Date(),new Date(), BigDecimal.valueOf(7.99),bus1, LocalTime.parse("06:30"),LocalTime.parse("08:30"));
        Trajet trajet1Bis =TrajetCreator.createTrajet(arret11,arret31,new Date(),new Date(), BigDecimal.valueOf(7.99),bus2, LocalTime.parse("11:45"),LocalTime.parse("14:05"));
        Trajet trajet1Ter =TrajetCreator.createTrajet(arret11,arret31,new Date(),new Date(), BigDecimal.valueOf(7.99),bus4, LocalTime.parse("15:20"),LocalTime.parse("17:50"));

        Trajet trajet2 =TrajetCreator.createTrajet(arret13,arret23,new Date(),new Date(), BigDecimal.valueOf(4.99),bus2, LocalTime.parse("10:50"),LocalTime.parse("12:30"));

        Trajet trajet3 =TrajetCreator.createTrajet(arret21,arret31,new Date(),new Date(), BigDecimal.valueOf(11.99),bus3, LocalTime.parse("09:30"),LocalTime.parse("11:35"));
        Trajet trajet4 =TrajetCreator.createTrajet(arret22,arret31,new Date(),new Date(), BigDecimal.valueOf(17.99),bus4, LocalTime.parse("13:20"),LocalTime.parse("15:50"));
        Trajet trajet5 =TrajetCreator.createTrajet(arret31,arret21,new Date(),new Date(), BigDecimal.valueOf(8.99),bus4, LocalTime.parse("16:30"),LocalTime.parse("18:30"));
        Trajet trajet6 =TrajetCreator.createTrajet(arret11,arret33,new Date(),new Date(), BigDecimal.valueOf(10.99),bus2, LocalTime.parse("19:30"),LocalTime.parse("22:20"));
        Trajet trajet7 =TrajetCreator.createTrajet(arret21,arret32,new Date(),new Date(), BigDecimal.valueOf(6.99),bus1, LocalTime.parse("14:05"),LocalTime.parse("16:25"));
        Trajet trajet8 =TrajetCreator.createTrajet(arret12,arret31,new Date(),new Date(), BigDecimal.valueOf(14.99),bus3, LocalTime.parse("07:30"),LocalTime.parse("09:50"));

        //trajetRepository.saveAll(Arrays.asList(trajet1,trajet2,trajet3,trajet4,trajet5,trajet6,trajet7,trajet8,trajet1Bis,trajet1Ter));

       // escaleRepository.saveAll(Arrays.asList(escale1,escale2));
        trajet2.setEscales(new HashSet<>(escaleList));
        //trajetRepository.save(trajet2);
    }
}
