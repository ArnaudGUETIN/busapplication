package com.tcv.bus.service;



import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;


@Component
public class MailConstructor {

    @Autowired
    private Environment env;
    @Autowired
    VelocityEngine velocityEngine;
    @Autowired
    private JavaMailSender mailSender;

    public void constructNewUserEmail() {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject("BusApp-Nouveau billet");
            mimeMessageHelper.setFrom(env.getProperty("support.email"));
            mimeMessageHelper.setTo("guetin.arnaud@gmail.com");
            Map model = new HashMap();
            model.put("title", "Arnaud GUETIN");
            model.put("mail", "guetin.arnaud@gmail.com");
            String text = VelocityEngineUtils.mergeTemplateIntoString(
                    velocityEngine, "/templates/demo.vm", model);
            mimeMessageHelper.setText(text, true);

            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

}
