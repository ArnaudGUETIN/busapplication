package com.tcv.bus.service;

import com.tcv.bus.entities.Ville;

import java.util.List;

public interface VilleService {
    Ville getOneVille(Long villeId);
    List<Ville> getAllViles();
}
