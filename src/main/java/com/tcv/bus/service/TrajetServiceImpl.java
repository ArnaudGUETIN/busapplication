package com.tcv.bus.service;

import com.tcv.bus.dao.*;
import com.tcv.bus.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
@Transactional
public class TrajetServiceImpl implements TrajetService {

    @Autowired
    TrajetRepository trajetRepository;
    @Autowired
    ArretRepository arretRepository;
    @Autowired
    BusRepository busRepository;
    @Autowired
    VoyageurRepository voyageurRepository;
    @Autowired
    EscaleRepository escaleRepository;

    @Override
    public List<Trajet> getAllArrets() {
        return trajetRepository.findAll();
    }

    @Override
    public Trajet getOneTrajet(Long idTrajet) {
        return trajetRepository.getOne(idTrajet);
    }

    @Override
    public Trajet addArretDeptToTrajet(Long arretDeptId, Long trajetId) {
        return null;
    }

    @Override
    public Trajet addArretArrToTrajet(Long arretArrtId, Long trajetId) {
        return null;
    }


    @Override
    public Trajet updateTrajet(Long trajetId, Trajet trajet,Long arretDepartId,Long arretArriveeId,Long busId) {
        Trajet oldTrajet = trajetRepository.getOne(trajetId);
        oldTrajet.setHeureDepart(trajet.getHeureDepart());
        oldTrajet.setHeureArrivee(trajet.getHeureArrivee());
        oldTrajet.setDateDepart(trajet.getDateDepart());
        oldTrajet.setDateArrivee(trajet.getDateArrivee());
        oldTrajet.setPrix(trajet.getPrix());
        oldTrajet.setDescription(trajet.getDescription());
        Arret arretDepart = arretRepository.getOne(arretDepartId);
        Arret arretArivee = arretRepository.getOne(arretArriveeId);
        Bus bus = busRepository.getOne(busId);
        oldTrajet.setArretDepart(arretDepart);
        oldTrajet.setArretArrivee(arretArivee);
        oldTrajet.setBus(bus);
        return trajetRepository.save(oldTrajet);
    }

    @Override
    public Escale addEscaleToTrajet(Long escaleId, Long trajetId) {
        Escale escale =escaleRepository.getOne(escaleId);
        Trajet trajet = trajetRepository.getOne(trajetId);
        escale.setTrajet(trajet);
        return escaleRepository.save(escale);
    }

    @Override
    public Escale createEscale(Long arretId, Escale escale) {
        Arret arret = arretRepository.getOne(arretId);
        escale.setArret(arret);

        return escaleRepository.save(escale);
    }

    @Override
    public List<Trajet> searchTrajets(Long idArretDepart, Long idArretArrivee,Date date) {
        List<Trajet> trajetList = new ArrayList<>();
        trajetRepository.findEscales(idArretDepart,idArretArrivee,date).forEach(t->{
             int stepDept=-1;
            int stepArr=-1;
            for(Escale escale:t.getEscales()){
                if(escale.getArret().getArretId().equals(idArretDepart)){
                    stepDept = escale.getStep();
                }
                if(escale.getArret().getArretId().equals(idArretArrivee)){
                    stepArr = escale.getStep();
                }
            }
            if(stepDept!=-1 && stepArr!=-1 && stepDept<stepArr){
                t.setDepartStep(stepDept);
                t.setArriveeStep(stepArr);
                trajetList.add(t);
            }

        });
        trajetList.addAll(trajetRepository.findAllByArretDepartArretIdAndArretArriveeArretIdAndDateDepart(idArretDepart,idArretArrivee,date));
        return trajetList;
    }

    public static Arret getArretDepart(Trajet trajet){
        Arret toReturn=null;
        if(trajet!= null  ){
            int stepDept = trajet.getDepartStep();


            for(Escale escale:trajet.getEscales()){
                if(escale.getStep()==stepDept){
                    return escale.getArret();
                }
            }

        }

        return toReturn;
    }

    public static Arret getArretArr(Trajet trajet){
        Arret toReturn=null;
        if(trajet!= null  ){
            int stepArr = trajet.getArriveeStep();


            for(Escale escale:trajet.getEscales()){
                if(escale.getStep()==stepArr){
                    return escale.getArret();
                }
            }

        }

        return toReturn;
    }

    @Override
    public Trajet createTrajet(Long arretDeptId, Long arretArrId, Long busId, @Temporal(TemporalType.DATE) Date dateDepart,  @Temporal(TemporalType.DATE)Date dateArrivee, LocalTime heureDepart, LocalTime heureArrivee) {
        Arret arretDept = arretRepository.getOne(arretDeptId);
        Arret arretArr =  arretRepository.getOne(arretArrId);
        Bus bus = busRepository.getOne(busId);
        Trajet trajet = new Trajet();
        trajet = trajetRepository.save(trajet);

        trajet.setArretDepart(arretDept);
        trajet.setArretArrivee(arretArr);
        trajet.setBus(bus);
        trajet.setDateDepart(dateDepart);
        trajet.setDateArrivee(dateArrivee);
        trajet.setHeureDepart(heureDepart);
        trajet.setHeureArrivee(heureArrivee);
        trajet.setArretArrReel(arretArr);
        trajet.setArretDepartReel(arretDept);
        return trajetRepository.save(trajet);
    }

    public static BigDecimal getPrixTrajet(Trajet trajet, BigDecimal prix){
        if(trajet== null  ) return null;
        BigDecimal toReturn = BigDecimal.ZERO;
        int stepDept = trajet.getDepartStep();
        int stepArr = trajet.getArriveeStep();


        for(Escale escale:trajet.getEscales()){
            if(escale.getStep()==stepDept){
                toReturn=prix.subtract(escale.getStepPrix());
            }
        }

        return toReturn;
    }
}
