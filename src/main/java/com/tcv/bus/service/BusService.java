package com.tcv.bus.service;

import com.tcv.bus.entities.Bus;

import java.util.List;

public interface BusService {

    List<Bus> getAllBus();
    Bus getOneBus(Long busId);
    Bus createBus(Bus bus);
}
