package com.tcv.bus.service;

import com.tcv.bus.dao.BusRepository;
import com.tcv.bus.entities.Bus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BusServiceImpl implements BusService {
    @Autowired
    private BusRepository busRepository;

    @Override
    public List<Bus> getAllBus() {
        return busRepository.findAll();
    }

    @Override
    public Bus createBus(Bus bus) {
        return busRepository.save(bus);
    }

    @Override
    public Bus getOneBus(Long busId) {
        return busRepository.getOne(busId);
    }
}
