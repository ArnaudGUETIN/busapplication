package com.tcv.bus.service;

import com.tcv.bus.entities.Billet;

import java.math.BigDecimal;
import java.util.List;

public interface BilletService {

    Billet getOneBillet(Long id);
    List<Billet> getAllBillet();
    List<Billet> getAllTrajetBillets(Long trajetId);
    Billet createBillet(String numeroBillet, BigDecimal reduction,Long trajetId);
    void sendBilletByMail(Long billetId);
    int generateNumeroBillet();
}
