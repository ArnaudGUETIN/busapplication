package com.tcv.bus.service;

import com.tcv.bus.dao.BilletRepository;
import com.tcv.bus.dao.TrajetRepository;
import com.tcv.bus.entities.Billet;
import com.tcv.bus.entities.Trajet;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class BilletServiceImpl implements BilletService {

    @Autowired
    private BilletRepository billetRepository;
    @Autowired
    private TrajetRepository trajetRepository;
    @Autowired
    private MailConstructor mailConstructor;


    @Override
    public Billet getOneBillet(Long id) {
        return billetRepository.getOne(id);
    }

    @Override
    public List<Billet> getAllBillet() {
        return billetRepository.findAll();
    }

    @Override
    public List<Billet> getAllTrajetBillets(Long trajetId) {
        List<Billet> billetList = new ArrayList<>();
        Trajet trajet = trajetRepository.getOne(trajetId);
        if(trajet!=null){
            billetList = new ArrayList<>(trajet.getBillets());
        }
        return billetList;
    }

    @Override
    public int generateNumeroBillet() {
        int size = billetRepository.findAll().size();
        return size;
    }

    @Override
    public void sendBilletByMail(Long billetId) {
        mailConstructor.constructNewUserEmail();



    }

    @Override
    public Billet createBillet(String numeroBillet, BigDecimal reduction, Long trajetId) {
        Billet  billet = new Billet();
        billet.setTrajet(trajetRepository.getOne(trajetId));
        billet.setReduction(reduction);
        billet.setNumeroBillet("#B00"+generateNumeroBillet());
        sendBilletByMail(0L);
        return billetRepository.save(billet);
    }
}
