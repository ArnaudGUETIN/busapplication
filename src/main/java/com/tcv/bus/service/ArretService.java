package com.tcv.bus.service;

import com.tcv.bus.entities.Arret;

import java.util.List;

public interface ArretService {

    List<Arret> getAllArrets();
    Arret getOneArret(Long idArret);
    Arret createArret(Long villeId,String nom,Double longitude,Double latitude);
    Arret updateArret(Long arretId,Arret arret);

}
