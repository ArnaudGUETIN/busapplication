package com.tcv.bus.service;

import com.tcv.bus.dao.VilleRepository;
import com.tcv.bus.entities.Ville;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VilleServiceImpl implements VilleService {

    @Autowired
    private VilleRepository villeRepository;
    @Override
    public Ville getOneVille(Long villeId) {
        return villeRepository.getOne(villeId);
    }

    @Override
    public List<Ville> getAllViles() {
        return villeRepository.findAll();
    }
}
