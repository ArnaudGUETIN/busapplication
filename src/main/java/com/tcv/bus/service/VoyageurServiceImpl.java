package com.tcv.bus.service;

import com.tcv.bus.dao.TrajetRepository;
import com.tcv.bus.dao.VoyageurRepository;
import com.tcv.bus.entities.Billet;
import com.tcv.bus.entities.Trajet;
import com.tcv.bus.entities.Voyageur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Transactional
@Service
public class VoyageurServiceImpl implements VoyageurService{
    @Autowired
    private VoyageurRepository voyageurRepository;
    @Autowired
    private TrajetRepository trajetRepository;
    @Autowired
    private BilletService billetService;
    @Override
    public Voyageur getVoyageur(Long id) {
        return voyageurRepository.getOne(id);
    }

    @Override
    public List<Voyageur> getAllVoyageurs() {
        return voyageurRepository.findAll();
    }

    @Override
    public Voyageur addVoyageur(Voyageur voyageur) {
        return voyageurRepository.save(voyageur);
    }

    @Override
    public Voyageur addBilletToVoyageur(Long billetId, Long voyageurId) {
        return null;
    }

    @Override
    public Voyageur addVoyageurToTrajet(Long voyageurId, Long trajetId) {
        Voyageur voyageur = voyageurRepository.getOne(voyageurId);
        Trajet trajet = trajetRepository.getOne(trajetId);
        Billet billet = billetService.createBillet("", BigDecimal.ONE,trajetId);
        if(voyageur!=null){
            voyageur.setTrajet(trajet);
            voyageur.setArretDepart(trajet.getArretDepart());
            voyageur.setArretArrive(trajet.getArretArrivee());
            voyageur = voyageurRepository.save(voyageur);
            voyageur.setBillet(billet);
        }


        return voyageurRepository.save(voyageur);
    }
}
