package com.tcv.bus.service;

import com.tcv.bus.entities.Trajet;
import com.tcv.bus.entities.Voyageur;

import java.util.List;

public interface VoyageurService {
    Voyageur getVoyageur(Long id);
    List<Voyageur> getAllVoyageurs();
    Voyageur addVoyageur(Voyageur voyageur);
    Voyageur addVoyageurToTrajet(Long voyageurId, Long trajetId);
    Voyageur addBilletToVoyageur(Long billetId,Long voyageurId);
}
