package com.tcv.bus.service;

import com.tcv.bus.dao.ArretRepository;
import com.tcv.bus.dao.VilleRepository;
import com.tcv.bus.entities.Arret;
import com.tcv.bus.entities.Ville;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class ArretServiceImpl implements ArretService {

    @Autowired
    private ArretRepository arretRepository;
    @Autowired
    private VilleRepository villeRepository;

    @Override
    public List<Arret> getAllArrets() {
        return arretRepository.findAll();
    }

    @Override
    public Arret updateArret(Long arretId, Arret arret) {
        Arret oldArret = arretRepository.getOne(arretId);
        oldArret.setLatitude(arret.getLatitude());
        oldArret.setNom(arret.getNom());
        oldArret.setCodePostal(arret.getCodePostal());
        oldArret.setLongitude(arret.getLongitude());
        oldArret.setDescription(arret.getDescription());
        oldArret.setPays(arret.getPays());
        return arretRepository.save(oldArret);
    }

    @Override
    public Arret createArret(Long villeId, String nom, Double longitude, Double latitude) {
        Ville ville = villeRepository.getOne(villeId);
        Arret arret = new Arret();
        arret.setNom(nom);
        arret.setLongitude(longitude);
        arret.setLatitude(latitude);
        arret.setVille(ville);
        return arretRepository.save(arret);
    }

    @Override
    public Arret getOneArret(Long idArret) {
        return arretRepository.getOne(idArret);
    }
}
