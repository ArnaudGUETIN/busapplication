package com.tcv.bus.service;

import com.tcv.bus.entities.Escale;
import com.tcv.bus.entities.Trajet;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

public interface TrajetService {

    List<Trajet> getAllArrets();
    Trajet getOneTrajet(Long idTrajet);
    Trajet createTrajet(Long arretDeptId, Long arretArrId, Long busId, Date dateDepart, Date dateArrivee, LocalTime heureDepart,LocalTime heureArrivee);
    Trajet addArretDeptToTrajet(Long arretDeptId,Long trajetId);
    Trajet addArretArrToTrajet(Long arretArrtId,Long trajetId);
    List<Trajet> searchTrajets(Long idArretDepart, Long idTrajetArrivee, Date date);
    Escale createEscale(Long arretId,Escale escale);
    Escale addEscaleToTrajet(Long escaleId,Long trajetId);
    Trajet updateTrajet(Long trajetId,Trajet trajet,Long arretDepartId,Long arretArriveeId,Long busId);
}
