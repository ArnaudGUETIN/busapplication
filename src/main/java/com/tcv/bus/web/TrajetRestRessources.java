package com.tcv.bus.web;

import com.tcv.bus.dao.ArretRepository;
import com.tcv.bus.dao.TrajetRepository;
import com.tcv.bus.entities.Arret;
import com.tcv.bus.entities.Escale;
import com.tcv.bus.entities.Trajet;
import com.tcv.bus.entities.Voyageur;
import com.tcv.bus.service.TrajetService;
import com.tcv.bus.service.VoyageurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/tcv")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TrajetRestRessources {

    @Autowired
    TrajetRepository trajetRepository;
    @Autowired
    ArretRepository arretRepository;
    @Autowired
    TrajetService trajetService;
    @Autowired
    VoyageurService voyageurService;

    @RequestMapping(method = RequestMethod.GET,value = "/allTrajets")
    public List<Trajet> getTrajetList(){
        return trajetRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.GET,value = "/trajet/{idTrajet}")
    public Trajet getOneTrajet(@PathVariable("idTrajet")Long idTrajet){
        return trajetRepository.getOne(idTrajet);
    }


    @RequestMapping(method = RequestMethod.GET,value = "/search")
    public List<Trajet> searchTrajets(@RequestParam("idArretDepart")Long idArretDepart,
                                      @RequestParam("idArretArrivee")Long idArretArrivee,
                                      @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)Date date){

        return trajetService.searchTrajets(idArretDepart,idArretArrivee,date);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/addVoyageur",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Voyageur addVoyageur(@RequestBody Voyageur voyageur){
        return voyageurService.addVoyageur( voyageur);
    }
    @RequestMapping(method = RequestMethod.GET,value = "/addVoyageur/{voyageurId}/ToTrajet/{trajetId}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Voyageur addVoyageurToTrajet(@PathVariable("voyageurId") Long voyageurId,@PathVariable("trajetId") Long trajetId){
        return voyageurService.addVoyageurToTrajet(voyageurId,trajetId);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/addTrajet")
    public void createTrajet(@RequestParam("arretDeptId") Long arretDeptId,
                               @RequestParam("arretArrId") Long arretArrId,
                               @RequestParam("busId")Long busId,@RequestBody Trajet trajet){

         trajetService.createTrajet(arretDeptId,arretArrId,busId,trajet.getDateDepart(),trajet.getDateArrivee(),trajet.getHeureDepart(),trajet.getHeureArrivee());

    }

    @RequestMapping(method = RequestMethod.POST,value = "/addEscale")
    public Escale createEscale(@RequestParam("arretId") Long arretId,
                             @RequestBody Escale escale){

        return trajetService.createEscale(arretId,escale);

    }

    @RequestMapping(method = RequestMethod.GET,value = "/addEscale/{escaleId}/toTrajet/{trajetId}")
    public Escale addEscaleToTrajet(@PathVariable("escaleId") Long escaleId,
                                    @PathVariable("trajetId") Long trajetId){

        return trajetService.addEscaleToTrajet(escaleId,trajetId);

    }

    @RequestMapping(method = RequestMethod.PUT,value = "/updateTrajet/{idTrajet}")
    public Trajet getArret(@PathVariable("idTrajet")Long idTrajet,@RequestBody Trajet trajet,@RequestParam("arretDepartId") Long arretDepartId,@RequestParam("arretArriveeId") Long arretArriveeId,@RequestParam("busId") Long busId){
        return trajetService.updateTrajet(idTrajet,trajet,arretDepartId,arretArriveeId,busId);
    }
}
