package com.tcv.bus.web;

import com.tcv.bus.entities.Bus;
import com.tcv.bus.service.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/tcv")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BusRestRessources {

    @Autowired
    private BusService busService;

    @RequestMapping(value = "/allBus",method = RequestMethod.GET)
    public List<Bus> getAllBus(){
        return busService.getAllBus();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/bus/{idBus}")
    public Bus getOneBus(@PathVariable("idBus")Long idBus){
        return busService.getOneBus(idBus);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/addBus")
    public Bus createBus(@RequestBody Bus bus){
        return busService.createBus(bus);
    }
}
