package com.tcv.bus.web;

import com.tcv.bus.entities.Ville;
import com.tcv.bus.service.VilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/tcv")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VilleRestRessources {

    @Autowired
    private VilleService villeService;

    @RequestMapping(value = "/ville/{villeId}",method = RequestMethod.GET)
    private Ville getOneVille(@PathVariable("villeId")Long villeId){
        return villeService.getOneVille(villeId);
    }

    @RequestMapping(value = "/allVilles",method = RequestMethod.GET)
    private List<Ville> getAllVilles(){
        return villeService.getAllViles();
    }
}
