package com.tcv.bus.web;

import com.tcv.bus.entities.Arret;
import com.tcv.bus.service.ArretService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/tcv")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ArretRestRessources {

    @Autowired
    private ArretService arretService;

    @RequestMapping(value = "/allArrets",method = RequestMethod.GET)
    public List<Arret> getAllArrets(){
        return arretService.getAllArrets();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/arret/{idArret}")
    public Arret getArret(@PathVariable("idArret")Long idArret){
        return arretService.getOneArret(idArret);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/addArret")
    public void createArret(@RequestParam("villeId") Long villeId,
                             @RequestBody Arret arret){

         arretService.createArret(villeId,arret.getNom(),arret.getLongitude(),arret.getLatitude());

    }

    @RequestMapping(method = RequestMethod.PUT,value = "/updateArret/{idArret}")
    public Arret getArret(@PathVariable("idArret")Long idArret,@RequestBody Arret arret){
        return arretService.updateArret(idArret,arret);
    }
}
