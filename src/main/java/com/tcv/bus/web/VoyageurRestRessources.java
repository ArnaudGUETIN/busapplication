package com.tcv.bus.web;

import com.tcv.bus.dao.VoyageurRepository;
import com.tcv.bus.entities.Voyageur;
import com.tcv.bus.service.VoyageurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/tcv")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VoyageurRestRessources {

    @Autowired
    private VoyageurService voyageurService;

    @RequestMapping(value = "/allVoyageurs",method = RequestMethod.GET)
    public List<Voyageur> getAllVoyageurs(){
        return voyageurService.getAllVoyageurs();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/voyageur/{idVoyageur}")
    public Voyageur getVoyageur(@PathVariable("idVoyageur")Long idVoyageur){
        return voyageurService.getVoyageur(idVoyageur);
    }
}
