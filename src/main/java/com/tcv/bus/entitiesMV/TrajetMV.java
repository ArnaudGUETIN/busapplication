package com.tcv.bus.entitiesMV;

import com.tcv.bus.entities.*;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

public class TrajetMV {

    private Long trajetId;
    private Date dateDepart;
    private Date dateArrivee;
    private BigDecimal prix;
    private Bus bus;
    private Arret arretDepart;
    private Arret arretArrivee;

    private LocalTime heureDepart;
    private LocalTime heureArrivee;

    private int departStep;

    private int arriveeStep;
    private long duree;
    /* Ce prix est calculé en fonction de l'arret de depart et d'arrivée des escales*/

    private long prixReelle;

}
