package com.tcv.bus.dao;

import com.tcv.bus.entities.Billet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BilletRepository extends JpaRepository<Billet,Long> {
}
