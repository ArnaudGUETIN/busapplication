package com.tcv.bus.dao;

import com.tcv.bus.entities.Voyageur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoyageurRepository extends JpaRepository<Voyageur,Long> {
}
