package com.tcv.bus.dao;

import com.tcv.bus.entities.Arret;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArretRepository extends JpaRepository<Arret,Long> {
}
