package com.tcv.bus.dao;

import com.tcv.bus.entities.Trajet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public interface TrajetRepository extends JpaRepository<Trajet,Long> {


    List<Trajet> findAllByArretDepartArretIdAndArretArriveeArretIdAndDateDepart(Long idArretDepart, Long idArretArrivee,@Temporal(TemporalType.DATE) Date date);
    @Query("SELECT t FROM Trajet t JOIN t.escales es join es.arret arr  WHERE  (arr.arretId=:idArretDepart or arr.arretId=:idArretArrivee) and es.dateDepart=:date")
    List<Trajet> findEscales(@Param("idArretDepart") Long idArretDepart,@Param("idArretArrivee") Long idArretArrivee, @Param("date")@Temporal(TemporalType.DATE) Date date);
}
