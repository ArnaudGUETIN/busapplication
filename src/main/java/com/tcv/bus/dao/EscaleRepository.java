package com.tcv.bus.dao;

import com.tcv.bus.entities.Escale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EscaleRepository extends JpaRepository<Escale,Long> {
}
