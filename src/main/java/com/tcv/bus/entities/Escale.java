package com.tcv.bus.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;
@Table(name = "t_escale")
@Entity
public class Escale implements Serializable {

    public static final int STEP_1=1;
    public static final int STEP_2=2;
    public static final int STEP_3=3;
    public static final int STEP_4=4;
    public static final int STEP_5=5;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long escaleId;
    @OneToOne(cascade = CascadeType.ALL)
    private Arret arret;
    @ManyToOne
    @JsonIgnore
    private Trajet trajet;
    @Temporal(TemporalType.DATE)
    private Date dateDepart;
    @Temporal(TemporalType.DATE)
    private Date dateArrivee;
    private LocalTime heureDepart;
    private LocalTime heureArrivee;
    private int step;
    private BigDecimal stepPrix;
    private String description;
    public Escale() {
    }

    public Long getEscaleId() {
        return escaleId;
    }

    public void setEscaleId(Long escaleId) {
        this.escaleId = escaleId;
    }

    public Arret getArret() {
        return arret;
    }

    public void setArret(Arret arret) {
        this.arret = arret;
    }

    public Trajet getTrajet() {
        return trajet;
    }

    public void setTrajet(Trajet trajet) {
        this.trajet = trajet;
    }

    public Date getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public LocalTime getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(LocalTime heureDepart) {
        this.heureDepart = heureDepart;
    }

    public LocalTime getHeureArrivee() {
        return heureArrivee;
    }

    public void setHeureArrivee(LocalTime heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public BigDecimal getStepPrix() {
        return stepPrix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStepPrix(BigDecimal stepPrix) {
        this.stepPrix = stepPrix;
    }
}
