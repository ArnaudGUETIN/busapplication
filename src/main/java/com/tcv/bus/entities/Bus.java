package com.tcv.bus.entities;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "t_bus")
@Entity
public class Bus implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long busId;
    private String numeroBus;
    private String Designation;
    private int nombrePlace;
    private Trajet trajet;

    public Bus() {
    }

    public Long getBusId() {
        return busId;
    }

    public void setBusId(Long busId) {
        this.busId = busId;
    }

    public String getNumeroBus() {
        return numeroBus;
    }

    public void setNumeroBus(String numeroBus) {
        this.numeroBus = numeroBus;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public int getNombrePlace() {
        return nombrePlace;
    }

    public void setNombrePlace(int nombrePlace) {
        this.nombrePlace = nombrePlace;
    }

    public Trajet getTrajet() {
        return trajet;
    }

    public void setTrajet(Trajet trajet) {
        this.trajet = trajet;
    }
}
