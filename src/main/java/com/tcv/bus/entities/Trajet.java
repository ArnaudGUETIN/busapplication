package com.tcv.bus.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tcv.bus.service.TrajetServiceImpl;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Table(name = "t_trajet")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trajet  implements Serializable {

    public static final int STEP_0=0;
    public static final int STEP_FIN=-1;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long trajetId;
    @Temporal(TemporalType.DATE)
    private Date dateDepart;
    @Temporal(TemporalType.DATE)
    private Date dateArrivee;
    private BigDecimal prix;
    @OneToOne(cascade = CascadeType.ALL)
    private Bus bus;
    @OneToOne(cascade = CascadeType.ALL)
    private Arret arretDepart;
    @OneToOne(cascade = CascadeType.ALL)
    private Arret arretArrivee;
    @OneToMany(mappedBy = "trajet")
    private Set<Voyageur> voyageurs;
    @OneToMany(mappedBy = "trajet",cascade = CascadeType.ALL)
    @OrderBy("step")
    private Set<Escale> escales;
    @OneToMany(mappedBy = "trajet",cascade = CascadeType.ALL)
    private Set<Billet> billets;
    private LocalTime heureDepart;
    private LocalTime heureArrivee;
    private String description;
    @Transient
    private int departStep = STEP_0;
    @Transient
    private int arriveeStep = STEP_FIN;
    @Transient
    private long duree;
    /* Ce prix est calculé en fonction de l'arret de depart et d'arrivée des escales*/
    @Transient
    private BigDecimal prixReelle;

    @Transient
    private Arret arretDepartReel;
    @Transient
    private Arret arretArrReel;
    public Trajet() {
    }

    public Long getTrajetId() {
        return trajetId;
    }

    public void setTrajetId(Long trajetId) {
        this.trajetId = trajetId;
    }

    public Date getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public Arret getArretDepart() {
        return arretDepart;
    }

    public void setArretDepart(Arret arretDepart) {
        this.arretDepart = arretDepart;
    }

    public Arret getArretArrivee() {
        return arretArrivee;
    }

    public void setArretArrivee(Arret arretArrivee) {
        this.arretArrivee = arretArrivee;
    }

    public Set<Escale> getEscales() {
        return escales;
    }

    public void setEscales(Set<Escale> escales) {
        this.escales = escales;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public Set<Billet> getBillets() {
        return billets;
    }

    public void setBillets(Set<Billet> billets) {
        this.billets = billets;
    }

    public LocalTime getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(LocalTime heureDepart) {
        this.heureDepart = heureDepart;
    }

    public LocalTime getHeureArrivee() {
        return heureArrivee;

    }

    public void setHeureArrivee(LocalTime heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public long getDuree() {
        if(heureDepart!=null && heureArrivee!=null){
            duree = heureArrivee.toSecondOfDay()-heureDepart.toSecondOfDay();
        }
        return duree;
    }

    public void setDuree(long duree) {
        this.duree = duree;
    }

    public Set<Voyageur> getVoyageurs() {
        return voyageurs;
    }

    public void setVoyageurs(Set<Voyageur> voyageurs) {
        this.voyageurs = voyageurs;
    }

    public int getDepartStep() {
        return departStep;
    }

    public void setDepartStep(int departStep) {
        this.departStep = departStep;
    }

    public int getArriveeStep() {
        return arriveeStep;
    }

    public void setArriveeStep(int arriveeStep) {
        this.arriveeStep = arriveeStep;
    }

    public BigDecimal getPrixReelle() {
        BigDecimal prix = getPrix();
        if (getDepartStep() == STEP_0 || TrajetServiceImpl.getPrixTrajet(this, prix) == null) {
            prixReelle = prix;
        } else {
            prixReelle = TrajetServiceImpl.getPrixTrajet(this, prix);
        }
        return prixReelle;
    }

    public void setPrixReelle(BigDecimal prixReelle) {
        this.prixReelle = prixReelle;
    }

    public Arret getArretDepartReel() {
        if (TrajetServiceImpl.getArretDepart(this) == null || getDepartStep() == STEP_0) {
            arretDepartReel = getArretDepart();
        } else {
            arretDepartReel = TrajetServiceImpl.getArretDepart(this);
        }

        return arretDepartReel;
    }

    public void setArretDepartReel(Arret arretDepartReel) {
        this.arretDepartReel = arretDepartReel;
    }

    public Arret getArretArrReel() {
        if (TrajetServiceImpl.getArretArr(this) == null || getArriveeStep() == STEP_FIN) {
            arretArrReel = getArretArrivee();
        } else {
            arretArrReel = TrajetServiceImpl.getArretArr(this);
        }

        return arretArrReel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setArretArrReel(Arret arretArrReel) {
        this.arretArrReel = arretArrReel;
    }
}
