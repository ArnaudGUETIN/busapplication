package com.tcv.bus.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "t_billet")
@Entity
public class Billet implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long billetid;
    private String numeroBillet;
    private BigDecimal reduction;
    @ManyToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private Trajet trajet;

    public Billet() {
    }

    public Long getBilletid() {
        return billetid;
    }

    public void setBilletid(Long billetid) {
        this.billetid = billetid;
    }

    public String getNumeroBillet() {
        return numeroBillet;
    }

    public void setNumeroBillet(String numeroBillet) {
        this.numeroBillet = numeroBillet;
    }

    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public Trajet getTrajet() {
        return trajet;
    }

    public void setTrajet(Trajet trajet) {
        this.trajet = trajet;
    }
}
