package com.tcv.bus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Table(name = "t_ville")
@Entity
public class Ville implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long villeId;
    private String nom;
    @OneToMany(mappedBy = "ville",cascade = CascadeType.ALL)
    private Set<Arret> arrets;

    public Ville() {
    }

    public Long getVilleId() {
        return villeId;
    }

    public void setVilleId(Long villeId) {
        this.villeId = villeId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Arret> getArrets() {
        return arrets;
    }

    public void setArrets(Set<Arret> arrets) {
        this.arrets = arrets;
    }
}
