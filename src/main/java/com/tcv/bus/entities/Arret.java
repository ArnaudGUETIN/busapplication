package com.tcv.bus.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "t_arret")
@Entity
public class Arret implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long arretId;
    private String nom;
    private Double longitude;
    private Double latitude;
    private String pays;
    private int codePostal;
    private String description;
    @Transient
    private String nomVille;
    @ManyToOne
    @JsonIgnore
    private Ville ville;
    public Long getArretId() {
        return arretId;
    }

    public Arret() {
    }

    public void setArretId(Long arretId) {
        this.arretId = arretId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getNomVille() {
        if(getVille()!=null){
            nomVille = getVille().getNom();       }
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }
}
