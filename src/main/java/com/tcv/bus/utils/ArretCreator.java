package com.tcv.bus.utils;

import com.tcv.bus.entities.Arret;
import com.tcv.bus.entities.Trajet;
import com.tcv.bus.entities.Ville;

public class ArretCreator {

    public static Arret createArret(Ville ville, Double latitude, Double longitude, String nom){
        Arret arret = new Arret();
        arret.setLatitude(latitude);
        arret.setLongitude(longitude);
        arret.setNom(nom);
        arret.setVille(ville);

        return arret;
    }
}
