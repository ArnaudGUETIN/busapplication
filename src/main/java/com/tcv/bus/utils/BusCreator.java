package com.tcv.bus.utils;

import com.tcv.bus.entities.Bus;

public class BusCreator {

    public static Bus createBus(String designation,int nbrePlace,String numerBus){
        Bus bus = new Bus();
        bus.setDesignation(designation);
        bus.setNombrePlace(nbrePlace);
        bus.setNumeroBus(numerBus);

        return bus;
    }
}
