package com.tcv.bus.utils;

import com.tcv.bus.entities.Arret;
import com.tcv.bus.entities.Bus;
import com.tcv.bus.entities.Trajet;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;

public class TrajetCreator {

    public static Trajet createTrajet(Arret arretdepart, Arret arretArrivee, Date dateDepart, Date dateArrivee, BigDecimal prix, Bus bus, LocalTime heureDepart,LocalTime heureArrivee){
        Trajet trajet = new Trajet();
        trajet.setArretDepart(arretdepart);
        trajet.setArretArrivee(arretArrivee);
        trajet.setDateDepart(dateDepart);
        trajet.setDateArrivee(dateArrivee);
        trajet.setPrix(prix);
        trajet.setBus(bus);
        trajet.setHeureDepart(heureDepart);
        trajet.setHeureArrivee(heureArrivee);

        return trajet;
    }
}
