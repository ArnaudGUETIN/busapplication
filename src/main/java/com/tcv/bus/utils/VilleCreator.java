package com.tcv.bus.utils;

import com.tcv.bus.entities.Ville;

public class VilleCreator {

    public static Ville createVille(String nom){
        Ville ville = new Ville();
        ville.setNom(nom);
        return ville;
    }
}
