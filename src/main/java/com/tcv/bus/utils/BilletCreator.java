package com.tcv.bus.utils;

import com.tcv.bus.entities.Billet;
import com.tcv.bus.entities.Trajet;

import java.math.BigDecimal;

public class BilletCreator {

    public static Billet createBillet(Trajet trajet, String numeroBillet, BigDecimal reduction){
        Billet billet = new Billet();
        billet.setNumeroBillet(numeroBillet);
        billet.setReduction(reduction);
        billet.setTrajet(trajet);

        return billet;
    }
}
